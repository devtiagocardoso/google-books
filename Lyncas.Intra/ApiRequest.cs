﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Lyncas.Intra
{
    public class ApiRequest
    {
        public static string UrlGoogleBooks { set; get; } = "https://www.googleapis.com/books/v1/volumes?q=[NOMEPESQUISA]&startIndex=[PAGE]&maxResults=40&filter=ebooks";

        public string Post(string Palavra, string Pagina)
        {
            string retorno = "";

            using (var objClint = new HttpClient())
            {
                objClint.BaseAddress = new Uri(UrlGoogleBooks.Replace("[NOMEPESQUISA]", Palavra).Replace("[PAGE]", Pagina));
                objClint.DefaultRequestHeaders.Accept.Clear();
                objClint.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
              
                HttpContent request = new StringContent("", Encoding.UTF8, "application/json");
                var response = objClint.GetAsync(objClint.BaseAddress).Result;

                if (response.IsSuccessStatusCode)
                {
                    retorno = response.Content.ReadAsStringAsync().Result;
                }
            }

            return retorno;
        }
    }
}
