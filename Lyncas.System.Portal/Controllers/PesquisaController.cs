﻿using Lyncas.Intra;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Lyncas.System.Portal.Controllers
{
    public class PesquisaController : Controller
    {
        [HttpPost]
        public ActionResult Livro(string Livro, string Pagina)
        {
            try
            {
                if (String.IsNullOrEmpty(Pagina))
                {
                    Pagina = "0";
                }
                var request = new ApiRequest().Post(Livro, Pagina);

                var lstObject = new List<string>();
                lstObject.Add(JsonConvert.SerializeObject(request));

                return Json(lstObject.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception Error)
            {
                return Json(Error.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}