﻿using System.Web.Mvc;

namespace Lyncas.System.Portal.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}