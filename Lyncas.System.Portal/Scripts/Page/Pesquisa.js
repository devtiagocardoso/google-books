﻿console.group("CARREGANDO MODULO PESQUISA");

$(document).ready(function () {
    $("#pagina").change(function () {
        PesquisarLivro();
    });

    $("#nomeLivro").keypress(function (e) {
        if (e.keyCode == 13) {
            PesquisarLivro();
        }
    });
});

function PesquisarLivro() {
    if ($("#nomeLivro").val() == "") {
        $(".modal-body").html("<i class=\"fas fa-times\"></i> Digite um nome de um livro para pesquisa");
        $('.modal').show();
    }
    else {
        $("#loadingPage").fadeIn();
        $("#pagina-resultado").show();

        var pagina = $("#pagina").val();

        var Params = [];
        Params.push(pagina);

        JAjaxSync("POST", "/Pesquisa/Livro", JSON.stringify({ Livro: $("#nomeLivro").val(), Pagina: pagina }), null, null, PesquisarLivroDetalhes, Params);
    }
}

function PesquisarLivroDetalhes(data) {
    var retorno = "";

    $.each(data, function (i, item) {
        item = JSON.parse(JSON.parse(item));
        if (item.totalItems > 0) {
            $.each(item.items, function (a, items) {
                var id = "", titulo = "", imagem = "", descri = "", preco = "", moeda = "", link = "";

                try { id = items.id; } catch (e) { }

                try { titulo = items.volumeInfo.title.replaceAll("\"", "").replaceAll("'", ""); } catch (e) { }

                try { imagem = items.volumeInfo.imageLinks.smallThumbnail; } catch (e) { }

                try { descri = items.volumeInfo.description.replaceAll("\"", "").replaceAll("'", ""); } catch (e) { }

                try { preco = items.saleInfo.listPrice.amount; } catch (e) { }

                try { moeda = items.saleInfo.listPrice.currencyCode; } catch (e) { }

                try { link = items.saleInfo.buyLink; } catch (e) { }

                

                retorno += "<h2>" + titulo + "</h2>";
                retorno += "<div class='div-flex'>";
                retorno += "<div><img src='" + imagem + "' /></div>";
                retorno += "<div><p>" + descri + "</p></div>";
                retorno += "</div>";
                retorno += "<h5>$ " + preco + " " + moeda + " <a class='btn btn-success' href='" + link + "' target='_blank'><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i> COMPRAR</a> <a onclick='MeusLivrosFavoritos(\"" + id + "\", \"" + titulo + "\", \"" + imagem + "\", \"" + descri + "\", \"" + preco + "\", \"" + moeda + "\", \"" + link + "\")' class='btn btn-default " + ClasseMeusFavoritos(id) + "'><i class=\"fas fa-star\"></i></a></h5>";
            });
        }
    });

    $("#resultado-final").html(retorno);
    $("#loadingPage").fadeOut();
}