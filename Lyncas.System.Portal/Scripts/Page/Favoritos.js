﻿console.group("CARREGANDO MODULO FAVORITOS");

$(document).ready(function () {
    ListarMeusFavoritos();
});

function ListarMeusFavoritos() {
    $("#loadingPage").fadeIn();
    var listaLivros = JSON.parse(sessionStorage.getItem("testObject")), retorno = "";
    if (listaLivros.length > 0) {
        for (i = 0; i < listaLivros.length; i++) {
            for (var key in listaLivros[i]) {
                if (key == "Id") {
                    retorno += "<h2>" + listaLivros[i].Titulo + "</h2>";
                    retorno += "<div class='div-flex'>";
                    retorno += "<div><img src='" + listaLivros[i].Imagem + "' /></div>";
                    retorno += "<div><p>" + listaLivros[i].Descricao + "</p></div>";
                    retorno += "</div>";
                    retorno += "<h5>$ " + listaLivros[i].Preco + " " + listaLivros[i].Moeda + " <a class='btn btn-success' href='" + listaLivros[i].Link + "' target='_blank'><i class=\"fa fa-shopping-cart\" aria-hidden=\"true\"></i> COMPRAR</a> <a onclick='MeusLivrosFavoritos(\"" + listaLivros[i].Id + "\", \"" + listaLivros[i].Titulo + "\", \"" + listaLivros[i].Imagem + "\", \"" + listaLivros[i].Descricao + "\", \"" + listaLivros[i].Preco + "\", \"" + listaLivros[i].Moeda + "\", \"" + listaLivros[i].Link + "\")' class='btn btn-default favorito'><i class=\"fas fa-star\"></i></a></h5>";
                }
            }
        }
    }
    else {
        retorno += "<h3 class='titulo'><i class=\"fas fa-book\"></i> Nenhum livro foi marcado como favorito</h3>";
    }

    $("#resultado-final").html(retorno);
    $("#loadingPage").fadeOut();
}
