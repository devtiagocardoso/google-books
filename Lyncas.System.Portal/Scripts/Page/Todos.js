﻿$(document).ready(function () {
    $('[aria-label="Close"]').click(function () {
        $('.modal').hide();
    });
});

function MeusLivrosFavoritos(_Id, _Titulo, _Imagem, _Descricao, _Preco, _Moeda, _Link) {
    var listaLivros = JSON.parse(sessionStorage.getItem("testObject"));
    if (listaLivros != null) {
        var existe = false;

        for (i = 0; i < listaLivros.length; i++) {
            for (var key in listaLivros[i]) {
                if (key == "Id") {
                    if (listaLivros[i].Id == _Id) {
                        listaLivros.splice(i, 1);
                        existe = true;
                    }
                }
            }
        }

        if (existe) {
            sessionStorage.setItem('testObject', JSON.stringify(listaLivros));
            $(".modal-body").html("<i class=\"fas fa-times\"></i> Livro removido dos meus favoritos");
            $('.modal').show();

            if (parseInt($("h2:contains(Favoritos)").length) > 0) {
                ListarMeusFavoritos();
            }
        }
        else {
            AdicionarLivro(_Id, _Titulo, _Imagem, _Descricao, _Preco, _Moeda, _Link);
        }
    }
    else {
        AdicionarLivro(_Id, _Titulo, _Imagem, _Descricao, _Preco, _Moeda, _Link);
    }

    PesquisarLivro();
}

function AdicionarLivro(_Id, _Titulo, _Imagem, _Descricao, _Preco, _Moeda, _Link) {
    var listaLivros = JSON.parse(sessionStorage.getItem("testObject"));

    // Se for a primeira vez gravando em sessão transformar variavel em array
    if (listaLivros == null) {
        listaLivros = [];
    }

    var dict = {
        Id: _Id,
        Titulo: _Titulo,
        Imagem: _Imagem,
        Descricao: _Descricao,
        Preco: _Preco,
        Moeda: _Moeda,
        Link: _Link
    }
    listaLivros.push(dict);
    sessionStorage.setItem('testObject', JSON.stringify(listaLivros));
    $(".modal-body").html("<i class=\"fas fa-check\"></i> Livro adicionado em meus favoritos");
    $('.modal').show();
}

function ClasseMeusFavoritos(id) {
    var classfav = "normal";
    var listaLivros = JSON.parse(sessionStorage.getItem("testObject"));
    if (listaLivros != null) {
        for (i = 0; i < listaLivros.length; i++) {
            for (var key in listaLivros[i]) {
                if (key == "Id") {
                    if (listaLivros[i].Id == id) {
                        classfav = "favorito";
                    }
                }
            }
        }
    }
    return classfav;
}