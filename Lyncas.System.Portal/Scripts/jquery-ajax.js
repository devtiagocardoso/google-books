﻿function JAjax(_Type, _Url, _Data, _DataError, _DataBeforeSend, SuccessCallback, parameters, ErrorCallback) {
    var _Retorno = "";
    var data;
    var textStatus;
    var jqXHR;
    $.ajax({
        type: _Type,
        url: _Url,
        data: _Data,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: true,
        context: parameters,
        success: SuccessCallback,
        error: ErrorCallback,
        beforeSend: _DataBeforeSend
    });
    return _Retorno;
}

function JAjaxSync(_Type, _Url, _Data, _DataError, _DataBeforeSend, _ReturnSucess, _Parameters) {
    $.ajax({
        type: _Type,
        url: _Url,
        data: _Data,
        contentType: 'application/json; charset=utf-8',
        datatype: 'json',
        async: false,
        success: _ReturnSucess,
        context: _Parameters,
        error: _DataError,
        beforeSend: _DataBeforeSend
    });
}

function JAjaxSyncRetorno(_Type, _Url, _Data, _DataError, _DataBeforeSend) {
    var _Retorno;
    $.ajax({
        type: _Type,
        url: _Url,
        data: _Data,
        contentType: 'application/json; charset=utf-8',
        datatype: 'json',
        async: false,
        success: function (data) {
            _Retorno = data;
        },
        error: function (jqXhr, textStatus, data) {
            if (_DataError == "console") {
                console.log(jqXhr);
            }
        },
        beforeSend: function () {
            if (_DataBeforeSend != null) {
                JsExecuteFunction(_DataBeforeSend);
            }
        }
    });
    return _Retorno;
}

function JsExecuteFunction(_Value) {
    eval(_Value);
}

function Log(_Value) {
    console.log(_Value);
}