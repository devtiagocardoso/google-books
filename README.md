# Teste para .Net C# Dev

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)
### Requisitos

- Google books (https://developers.google.com/books/)

### Detalhes do projeto

1. Foi criado 1 soluction com 3 projetos, contendo uma camada de conexão via httpclient como diz o requisito, outro projeto contendo o portal com 3 menus (Página Inicial, Pesquisa e Meus Favoritos) e o terceiro Projeto para teste unitário.

2. Criado dois novos controllers Pesquisa e Sistema

3. Desenvolvido em C# .NET Framework 4.7.2
3.1 MVC Padrão com front-end bootstrap
3.2 Dentro de scripts criado um arquivo jquery-ajax para fazer conexão em ajax
3.3 Criado uma pasta "Page" que são regras para cada tipo de tela

4. Não optei por usar um banco de dados local e sim o sessionStorage em javascript para simular um banco de dados no navegador do cliente

5. Por não usar banco de dados local os metodos delete, put, post(add), não foram criados, usei regras no front-end para que o mesmo possa gravar e deletar as informações conforme a solicitação do usuário

6. Foram gastas 08:10:00 horas para executar o projeto

### Desde já obrigado